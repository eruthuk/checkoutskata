﻿namespace Checkout
{
    using System;
    using System.Collections.Generic;

    public class Program
    {
        static void Main(string[] args)
        {
            // add simple display to screen 
            var itemList = new List<Item>();
            itemList.Add(new Item("A99", 0.5, new Discounts(3, 1.3)));
            itemList.Add(new Item("A99", 0.5, new Discounts(3, 1.3)));
            itemList.Add(new Item("C", 0.6));
            itemList.Add(new Item("A99", 0.5, new Discounts(3, 1.3)));
            itemList.Add(new Item("B", 0.3, new Discounts(2, 0.45)));
            itemList.Add(new Item("B", 0.3, new Discounts(2, 0.45)));
            itemList.Add(new Item("A99", 0.5, new Discounts(3, 1.3)));
            itemList.Add(new Item("C", 0.6));
            itemList.Add(new Item("B", 0.3, new Discounts(2, 0.45)));
            itemList.Add(new Item("C", 0.6));


            var check = new Checkout();

            foreach (var item in itemList)
            {
                check.Scan(item);
                Console.WriteLine("Total price is {0:0.00}", check.GetTotalPrice());
            }

            Console.WriteLine("Finished scanning");
            Console.WriteLine("Total price, with discounts is {0:0.00}", check.GetTotalPrice());
            Console.ReadLine();
        }
    }
}
