﻿namespace Checkout
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Item : IItem
    {
        public string SKU { get; }

        public double UnitPrice { get; }

        public IDiscount Discount { get; }

        public Item(string sku, double unitPrice, IDiscount discount = null)
        {
            if (unitPrice <= 0)
            {
                throw new ArgumentException("Unit price must be greater than zero", nameof(unitPrice));
            }

            SKU = sku;
            UnitPrice = unitPrice;
            Discount = discount;
        }
    }
}
