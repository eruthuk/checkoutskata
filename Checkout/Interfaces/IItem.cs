﻿namespace Checkout
{
    public interface IItem
    {
        string SKU { get; }
        double UnitPrice { get; }
        IDiscount Discount { get; }
    }
}
