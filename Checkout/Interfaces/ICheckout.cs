﻿namespace Checkout
{
    public interface ICheckout
    {
        void Scan(IItem item);
        double GetTotalPrice();
    }
}
