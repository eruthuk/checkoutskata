﻿namespace Checkout
{
    using System.Collections.Generic;

    public interface IDiscount
    {
        double CalculateDiscount(IEnumerable<IItem> items);
    }
}
