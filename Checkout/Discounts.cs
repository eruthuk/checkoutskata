﻿namespace Checkout
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    public class Discounts : IDiscount
    {
        public double ItemsRequired { get; }
        public double DiscountTotal { get; }

        public Discounts(int numberOfItems, double discountTotal)
        {
            if (numberOfItems <= 0)
                throw new ArgumentException("Discounts can only apply to one or more items", nameof(numberOfItems));

            if (discountTotal <= 0)
                throw new ArgumentException("Discount total must be greater than zero", nameof(discountTotal));

            ItemsRequired = numberOfItems;
            DiscountTotal = discountTotal;
        }

        public double CalculateDiscount(IEnumerable<IItem> items)
        {
            return (items.Count() / ItemsRequired) * DiscountTotal;
        }
    }
}
