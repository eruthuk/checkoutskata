﻿namespace Checkout
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Checkout : ICheckout
    {
        private List<IItem> _items = new List<IItem>();

        public Checkout()
        {

        }

        public void Scan(IItem item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            _items.Add(item);
        }

        public double GetTotalPrice()
        {
            var basketTotal = _items.Sum(i => i.UnitPrice);

            // Deduct any discounts from the sum total
            var discountedSkus = _items.Where(i => i.Discount != null).Select(i => i.SKU).Distinct();
            foreach (var sku in discountedSkus)
            {
                var skuItems = _items.Where(item => item.SKU == sku);
                if (skuItems.Any())
                {
                    basketTotal -= skuItems.First().Discount.CalculateDiscount(skuItems);
                }
            }

            return basketTotal;
        }

    }
}
