﻿namespace CheckoutTests
{
    using NUnit.Framework;
    using System;
    using Checkout;

    [TestFixture]
    public class CheckoutTests
    {
        [Test]
        public void ScanFailsWithNullExceptionIfNoItems()
        {
            // arrange
            var check = new Checkout();

            // act & assert
            var ex = Assert.Throws<ArgumentNullException>(
                () => check.Scan(null));
            Assert.That(ex.Message == "Value cannot be null.\r\nParameter name: item");
        }

        [Test]
        public void WhenNoItemsAreScannedTotalIsZero()
        {
            // arrange
            var check = new Checkout();

            // act & assert
            Assert.AreEqual(0, check.GetTotalPrice());
        }
    }
}
