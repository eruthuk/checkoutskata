﻿namespace CheckoutTests
{
    using Checkout;
    using NUnit.Framework;
    using System;

    public class ItemTest
    {
        [Test]
        public void ItemConstructsOKWithAllExpectedParameters()
        {
            // arrange
            string sku = "aaa";
            double unitPrice = 3.65;

            // act & assert
            Assert.DoesNotThrow(() => new Item(sku, unitPrice));
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-25.6)]
        public void ItemThrowsArgumentErrorWhenUnitPriceLessThanOne(double unitPrice)
        {
            // arrange
            string sku = "aaa";

            // act & assert
            var ex = Assert.Throws<ArgumentException>(
                () => new Item(sku, unitPrice));
            Assert.That(ex.Message == "Unit price must be greater than zero\r\nParameter name: unitPrice");
        }
    }
}
