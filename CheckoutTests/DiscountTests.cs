﻿namespace CheckoutTests
{
    using Checkout;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    [TestFixture]
    public class DiscountTests
    {
        [Test]
        public void WhenDiscountAppliedToLessThanOneItem_ExceptionThrown()
        {
            // act & assert
            var ex = Assert.Throws<ArgumentException>(
                () => new Discounts(0, 0));
            Assert.AreEqual(ex.Message, "Discounts can only apply to one or more items\r\nParameter name: numberOfItems");
        }

        [Test]
        public void WhenDiscountTotalIsLessThanOne_ExceptionThrown()
        {
            // arrange
            var numofItems = 3;

            // act & assert
            var ex = Assert.Throws<ArgumentException>(
                () => new Discounts(numofItems, 0));
            Assert.AreEqual(ex.Message, "Discount total must be greater than zero\r\nParameter name: discountTotal");
        }

        [Test]
        public void DiscountIsCalculatedCorrectly()
        {
            // arrange
            var numofItems = 3;
            var discountTotal = 3;
            var dis = new Discounts(numofItems,discountTotal);
            var item = new Item("a99", 0.5, dis);

            // act
            var discount = dis.CalculateDiscount(new List<Item> { item });

            // assert
            Assert.NotZero(discount);
            Assert.AreEqual(1, discount);
        }
    }
}
